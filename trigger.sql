--Creación de las tabla para llevar el registro de clientes, cuentas y transacciones con los trigger
create table reg_cliente(
	nombre varchar(20) NOT NULL PRIMARY KEY,
	fecha date NOT NULL,
	comentario varchar(300) NOT NULL)

create table reg_cuentas(
	numero varchar(100) NOT NULL PRIMARY KEY,
	fecha date NOT NULL,
	descripcion varchar(300) not null)

create table reg_trans(
	id varchar(25) NOT NULL PRIMARY KEY,
	fehca date NOT NULL,
	descripcion varchar(300) NOT NULL)

--Creacion de los trigger
create trigger registroInsert
on clientes
for insert --for |after |instead of insert | update | delete
as 
begin
insert into reg_cliente(nombre, fecha, comentario) select nombre, getdate(), 'se insertó un nuevo cliente' from inserted
end

create trigger registroUpdate
on clientes
after update
as 
begin
insert into reg_cliente(nombre, fecha, comentario) select nombre, getdate(), 'se actualizó un cliente' from inserted
end

create trigger registroDelte
on clientes
for delete
as 
begin
insert into reg_cliente(nombre, fecha, comentario) select nombre, getdate(), 'se eliminó un cliente' from inserted
end



create trigger cuenasInsert
on cuentas
for insert
as 
begin
insert into reg_cuentas(numero, fecha, descripcion) select numero, getdate(), 'se insertó una nueva cuenta' from inserted
end

create trigger cuentasUpdate
on cuentas
after update
as 
begin
insert into reg_cuentas(numero, fecha, descripcion) select numero, getdate(), 'se actualizó una cuenta' from inserted
end

create trigger cuentasDelte
on cuentas
for delete
as 
begin
insert into reg_cuentas(numero, fecha, descripcion) select numero, getdate(), 'se eliminó una cuenta' from inserted
end

create trigger transInsert
on transacciones
for insert
as 
begin
insert into reg_trans(id, fehca, descripcion) select id, getdate(), 'se insertó una nueva transacción' from inserted
end

create trigger transUpdate
on transacciones
after update
as 
begin
insert into reg_trans(id, fehca, descripcion) select id, getdate(), 'se actualizó una transacción' from inserted
end

create trigger transDelete
on transacciones
for delete
as 
begin
insert into reg_trans(id, fehca, descripcion) select id, getdate(), 'se eliminó una transacción' from inserted
end
