CREATE DATABASE gabinete
USE gabinete


CREATE TABLE clientes(
	id varchar(15) NOT NULL PRIMARY KEY,
	nombre varchar(25) NOT NULL,
	direccion varchar(25) NOT NULL,
	telefono varchar(25) NOT NULL,
	fecha_nacimiento DATE NOT NULL
)

CREATE TABLE procuradores(
	num_procurador varchar(15) NOT NULL PRIMARY KEY,
	nombre varchar(25) NOT NULL,
	salario money NOT NULL,
	fecha_ingreso DATE NOT NULL,
	fecha_nacimiento DATE NOT NULL
)

CREATE TABLE asunto(
	num_exp varchar(25) NOT NULL PRIMARY KEY,
	fecha_inicio DATE NOT NULL,
	fecha_archivado DATE NOT NULL,
	dni_cliente	varchar(15) NOT NULL ,
	procus varchar(15) NOT NULL,
	FOREIGN KEY (dni_cliente) REFERENCES clientes(id),
	FOREIGN KEY (procus) REFERENCES procuradores(num_procurador)
)
